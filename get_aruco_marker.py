#! /usr/bin/python

# Grabbing an object with an aruco marker attached to it


# rospy for the subscriber
import rospy
# ROS Geometry PoseStamped message
from geometry_msgs.msg import PoseStamped
from baxter_projectyouth import util
from baxter_projectyouth import my_bax_funcs as mf
import baxter_interface
import time
#rospy.init_node('aruco_listener')
util.connect_to_baxter('aruco_listener')
right = baxter_interface.Gripper('right')
right.calibrate()

#predesign by mf.check_if_configureable([.78,0,.37],[0,0,-1],'left')
_DEFAULT_CAMERA = [-72.2460936674468,
 -23.66455075420931,
 61.80908196062283,
 3.955078120480664,
 -59.17236321363572,
 112.47802721522511,
 -22.89550778633807]


default_out_of_way_camera = [-18.21533201043595,
 -24.52148434698012,
 46.999511665045226,
 3.977050776705557,
 -37.06787105139378,
 109.73144518711354,
 -15.84228513814755]

right_out_of_way = [6.262207024094385,
 -14.721679670678029,
 63.76464836463826,
 35.31005855340238,
 -57.348632746969635,
 82.17773428109825,
 17.81982419838788]

#default_y = -.0144
default_y = -.017
default_x = .0051
_DEFAULT_GOAL=[.8,-.2,-.1]
_DEFAULT_HEIGHT=.15
_DEFAULT_TABLE_DOWN=-.1
_SPEED = .3


util.set_right_arm_speed(.1)
util.set_left_arm_speed(.1)

def reset():
    # get back to default
    right.open()
    print "moving back to default"
    util.move_right_arm_to_positions(right_out_of_way)
    util.move_left_arm_to_positions(_DEFAULT_CAMERA)
    print "should be back at default"
    time.sleep(1)
    rospy.signal_shutdown('finished protocol')

def move_arm(goal,vec,limb):
    temp = mf.check_if_configureable(goal,vec,limb)
    if (temp is None):
        print "failed to meet desired workspace"
	reset()
    if (limb=='right'):
    	util.move_right_arm_to_positions(temp)
    else:
	util.move_left_arm_to_positions(temp)
    time.sleep(1.5)

def aruco_callback(msg):
    print("Attempting to move baxter")
    position = msg.pose.position
    quat = msg.pose.orientation
    new_x = .78 - 2.5*(-default_y+position.y)
    new_y = 0 - 2.5*(-default_x+position.x)
    # move camera out of the way
    util.move_left_arm_to_positions(default_out_of_way_camera)
    # go to object with right arm
    move_arm([new_x,new_y,_DEFAULT_HEIGHT],[0,0,-1],'right')
    move_arm([new_x,new_y,_DEFAULT_TABLE_DOWN],[0,0,-1],'right')
    right.close()
    time.sleep(.5)
    move_arm([new_x,new_y,_DEFAULT_HEIGHT],[0,0,-1],'right')
    # move object to new location
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_HEIGHT],[0,0,-1],'right')
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_TABLE_DOWN],[0,0,-1],'right')
    right.open()
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_HEIGHT],[0,0,-1],'right')
    reset()
    #global subscriber
    #subscriber.unregister()
    

def myhook():
    print "finished process"

def main():

    # Define your pose topic
    pose_topic = "/aruco_single/pose"
    # Set up your subscriber and define its callback
    rospy.Subscriber(pose_topic, PoseStamped, aruco_callback)
    #global subscriber 
    #subscriber= rospy.Subscriber(pose_topic, PoseStamped, aruco_callback)
    # Spin until ctrl + c
    rospy.spin()

if __name__ == '__main__':
    main()