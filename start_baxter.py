#!/usr/bin/env python

#start script
from baxter_projectyouth import util
import cv_bridge
import baxter_interface
from baxter_projectyouth import my_bax_funcs as mf
from std_srvs.srv import Empty
import pickle
import rospy

util.connect_to_baxter('start')
calibrate_fingers = rospy.ServiceProxy('/reflex_takktile/calibrate_fingers', Empty)
calibrate_tactile = rospy.ServiceProxy('/reflex_takktile/calibrate_tactile', Empty)
#pickle the value that no objects are in the hand
f = file("src/baxter_projectyouth/src/baxter_projectyouth/has_object_pickle",'wb')
pickle.dump(False,f,2)
f.close()

calibrate_fingers()
calibrate_tactile()
temp = mf.check_if_configureable([.78,0.0,.37],[0,0,-1],'left')
#temp = mf.check_if_configureable([.7429,.207,-.05],[0,0,-1],'left')
#temp = mf.check_if_configureable([.78,0.0,-.1],[0,0,-1],'left')
util.move_left_arm_to_positions(temp)
#head_camera = baxter_interface.CameraController("head_camera")
#head_camera.close()
left_camera = baxter_interface.CameraController("left_hand_camera")
#left_camera.resolution=(1280,800)
left_camera.resolution=(640,400)
left_camera.open()
right_camera = baxter_interface.CameraController("right_hand_camera")
right_camera.resolution=(640,400)
right_camera.open()
