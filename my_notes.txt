Baxter range:

Need to determine range of Baxter end effector positions

even though the inverse kinematics solver says there is a solution there isn't really one at times becuase the robot's motors won't allow it certain configurations.


max x range is .85 to .3
max y range is -.9 to 3
max z range is -.45 to .3 though sometimes can go at .65

1,0,0
0,1,0

1,0,0


git add my_funcs.py
git commit -m "what I want to say"
git push -u bitbucket master

testing with cv2
python cv2
image=cv2.imread("test.png")
cv2.imshow("test",image)
cv2.waitKey(0)
http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_calib3d/py_calibration/py_calibration.html
http://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html


started roslaunch server http://baxter2control:47021/

SUMMARY
========

PARAMETERS
 * /camera/camera_nodelet_manager/num_worker_threads: 4
 * /camera/depth_rectify_depth/interpolation: 0
 * /camera/driver/auto_exposure: True
 * /camera/driver/auto_white_balance: True
 * /camera/driver/color_depth_synchronization: False
 * /camera/driver/depth_camera_info_url: 
 * /camera/driver/depth_frame_id: camera_depth_opti...
 * /camera/driver/depth_registration: False
 * /camera/driver/device_id: #1
 * /camera/driver/rgb_camera_info_url: 
 * /camera/driver/rgb_frame_id: camera_rgb_optica...
 * /rosdistro: indigo
 * /rosversion: 1.11.20

NODES
  /camera/
    camera_nodelet_manager (nodelet/nodelet)
    depth_metric (nodelet/nodelet)
    depth_metric_rect (nodelet/nodelet)
    depth_points (nodelet/nodelet)
    depth_rectify_depth (nodelet/nodelet)
    depth_registered_sw_metric_rect (nodelet/nodelet)
    driver (nodelet/nodelet)
    points_xyzrgb_sw_registered (nodelet/nodelet)
    register_depth_rgb (nodelet/nodelet)
    rgb_rectify_color (nodelet/nodelet)
  /
    camera_base_link (tf/static_transform_publisher)
    camera_base_link1 (tf/static_transform_publisher)
    camera_base_link2 (tf/static_transform_publisher)
    camera_base_link3 (tf/static_transform_publisher)

ROS_MASTER_URI=http://011305P0015:11311

core service [/rosout] found
process[camera/camera_nodelet_manager-1]: started with pid [1811]
process[camera/driver-2]: started with pid [1812]
process[camera/rgb_rectify_color-3]: started with pid [1813]
process[camera/depth_rectify_depth-4]: started with pid [1814]
process[camera/depth_metric_rect-5]: started with pid [1818]
process[camera/depth_metric-6]: started with pid [1822]
process[camera/depth_points-7]: started with pid [1827]
process[camera/register_depth_rgb-8]: started with pid [1831]
process[camera/points_xyzrgb_sw_registered-9]: started with pid [1841]
process[camera/depth_registered_sw_metric_rect-10]: started with pid [1845]
process[camera_base_link-11]: started with pid [1847]
[ INFO] [1475780458.497088747]: Initializing nodelet with 4 worker threads.
process[camera_base_link1-12]: started with pid [1857]
process[camera_base_link2-13]: started with pid [1862]
process[camera_base_link3-14]: started with pid [1871]
[ INFO] [1475780459.161773961]: Device "1d27/0601@3/38" found.
[ INFO] [1475780459.171531357]: No matching device found.... waiting for devices. Reason: openni2_wrapper::OpenNI2Device::OpenNI2Device(const string&) @ /tmp/binarydeb/ros-indigo-openni2-camera-0.2.7/src/openni2_device.cpp @ 74 : Device open failed
	Could not open "1d27/0601@3/38": Failed to open the USB device!


8018595858

header: 
  seq: 249
  stamp: 
    secs: 1475777847
    nsecs: 461371073
  frame_id: ''
level: 2
name: /camera/camera_nodelet_manager
msg: No matching device found.... waiting for devices. Reason: openni2_wrapper::OpenNI2Device::OpenNI2Device(const string&) @ /tmp/binarydeb/ros-indigo-openni2-camera-0.2.7/src/openni2_device.cpp @ 74 : Device open failed
	Could not open "1d27/0601@3/38": Failed to open the USB device!


file: /tmp/binarydeb/ros-indigo-openni2-camera-0.2.7/src/openni2_driver.cpp
function: OpenNI2Driver::initDevice
line: 738
topics: ['/rosout', '/camera/depth_rectify_depth/parameter_descriptions', '/camera/depth_rectify_depth/parameter_updates', '/camera/depth/image_rect_raw/compressedDepth', '/camera/depth/image_rect_raw/compressedDepth/parameter_descriptions', '/camera/depth/image_rect_raw/compressedDepth/parameter_updates', '/camera/depth/image_rect_raw/compressed', '/camera/depth/image_rect_raw/compressed/parameter_descriptions', '/camera/depth/image_rect_raw/compressed/parameter_updates', '/camera/depth/image_rect_raw', '/camera/depth/image_rect_raw/theora', '/camera/depth/image_rect_raw/theora/parameter_descriptions', '/camera/depth/image_rect_raw/theora/parameter_updates', '/camera/depth/points', '/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth', '/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect_raw/compressedDepth/parameter_updates', '/camera/depth_registered/sw_registered/image_rect_raw/compressed', '/camera/depth_registered/sw_registered/image_rect_raw/compressed/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect_raw/compressed/parameter_updates', '/camera/depth_registered/sw_registered/image_rect_raw', '/camera/depth_registered/sw_registered/image_rect_raw/theora', '/camera/depth_registered/sw_registered/image_rect_raw/theora/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect_raw/theora/parameter_updates', '/camera/depth_registered/sw_registered/camera_info', '/camera/depth_registered/points', '/camera/depth_registered/sw_registered/image_rect/compressedDepth', '/camera/depth_registered/sw_registered/image_rect/compressedDepth/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect/compressedDepth/parameter_updates', '/camera/depth_registered/sw_registered/image_rect/compressed', '/camera/depth_registered/sw_registered/image_rect/compressed/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect/compressed/parameter_updates', '/camera/depth_registered/sw_registered/image_rect', '/camera/depth_registered/sw_registered/image_rect/theora', '/camera/depth_registered/sw_registered/image_rect/theora/parameter_descriptions', '/camera/depth_registered/sw_registered/image_rect/theora/parameter_updates', '/camera/depth/image_rect/compressedDepth', '/camera/depth/image_rect/compressedDepth/parameter_descriptions', '/camera/depth/image_rect/compressedDepth/parameter_updates', '/camera/depth/image_rect/compressed', '/camera/depth/image_rect/compressed/parameter_descriptions', '/camera/depth/image_rect/compressed/parameter_updates', '/camera/depth/image_rect', '/camera/depth/image_rect/theora', '/camera/depth/image_rect/theora/parameter_descriptions', '/camera/depth/image_rect/theora/parameter_updates', '/camera/rgb_rectify_color/parameter_descriptions', '/camera/rgb_rectify_color/parameter_updates', '/camera/rgb/image_rect_color/compressedDepth', '/camera/rgb/image_rect_color/compressedDepth/parameter_descriptions', '/camera/rgb/image_rect_color/compressedDepth/parameter_updates', '/camera/rgb/image_rect_color/compressed', '/camera/rgb/image_rect_color/compressed/parameter_descriptions', '/camera/rgb/image_rect_color/compressed/parameter_updates', '/camera/rgb/image_rect_color', '/camera/rgb/image_rect_color/theora', '/camera/rgb/image_rect_color/theora/parameter_descriptions', '/camera/rgb/image_rect_color/theora/parameter_updates', '/camera/depth/image/compressedDepth', '/camera/depth/image/compressedDepth/parameter_descriptions', '/camera/depth/image/compressedDepth/parameter_updates', '/camera/depth/image/compressed', '/camera/depth/image/compressed/parameter_descriptions', '/camera/depth/image/compressed/parameter_updates', '/camera/depth/image', '/camera/depth/image/theora', '/camera/depth/image/theora/parameter_descriptions', '/camera/depth/image/theora/parameter_updates']

baxter2control 287 indigo_ws $ rostopic list
/ExternalTools/left/PositionKinematicsNode/FKServer/reply
/ExternalTools/left/PositionKinematicsNode/FKServer/request
/ExternalTools/left/PositionKinematicsNode/IKServer/reply
/ExternalTools/left/PositionKinematicsNode/IKServer/request
/ExternalTools/right/PositionKinematicsNode/FKServer/reply
/ExternalTools/right/PositionKinematicsNode/FKServer/request
/ExternalTools/right/PositionKinematicsNode/IKServer/reply
/ExternalTools/right/PositionKinematicsNode/IKServer/request
/cameras/left_hand_camera/camera_info
/cameras/left_hand_camera/camera_info_std
/cameras/left_hand_camera/image
/cameras/right_hand_camera/camera_info
/cameras/right_hand_camera/camera_info_std
/cameras/right_hand_camera/image
/collision/left/collision_detection
/collision/left/debug
/collision/right/collision_detection
/collision/right/debug
/diagnostics
/diagnostics_agg
/diagnostics_toplevel_state
/hdraw
/joint_trajectory_contact_sensor/left/debug
/joint_trajectory_contact_sensor/right/debug
/robot/accelerometer/left_accelerometer/state
/robot/accelerometer/right_accelerometer/state
/robot/accelerometer_names
/robot/accelerometer_states
/robot/analog_io/command
/robot/analog_io/left_hand_range/state
/robot/analog_io/left_hand_range/value_uint32
/robot/analog_io/left_itb_wheel/state
/robot/analog_io/left_itb_wheel/value_uint32
/robot/analog_io/left_vacuum_sensor_analog/state
/robot/analog_io/left_vacuum_sensor_analog/value_uint32
/robot/analog_io/right_hand_range/state
/robot/analog_io/right_hand_range/value_uint32
/robot/analog_io/right_itb_wheel/state
/robot/analog_io/right_itb_wheel/value_uint32
/robot/analog_io/right_vacuum_sensor_analog/state
/robot/analog_io/right_vacuum_sensor_analog/value_uint32
/robot/analog_io/torso_fan/state
/robot/analog_io/torso_fan/value_uint32
/robot/analog_io/torso_left_itb_wheel/state
/robot/analog_io/torso_left_itb_wheel/value_uint32
/robot/analog_io/torso_lighting/state
/robot/analog_io/torso_lighting/value_uint32
/robot/analog_io/torso_right_itb_wheel/state
/robot/analog_io/torso_right_itb_wheel/value_uint32
/robot/analog_io_names
/robot/analog_io_states
/robot/assembly/head/set_components_enable
/robot/assembly/head/state
/robot/assembly/left/set_components_enable
/robot/assembly/left/state
/robot/assembly/right/set_components_enable
/robot/assembly/right/state
/robot/assembly/torso/set_components_enable
/robot/assembly/torso/state
/robot/assembly_names
/robot/attached_collision_object
/robot/collision_object
/robot/digital_io/command
/robot/digital_io/head_green_light/state
/robot/digital_io/head_lcd_auto_config/state
/robot/digital_io/head_red_light/state
/robot/digital_io/head_yellow_light/state
/robot/digital_io/left_blow/state
/robot/digital_io/left_hand_camera_power/state
/robot/digital_io/left_itb_button0/state
/robot/digital_io/left_itb_button1/state
/robot/digital_io/left_itb_button2/state
/robot/digital_io/left_itb_button3/state
/robot/digital_io/left_itb_button_down/state
/robot/digital_io/left_itb_button_left/state
/robot/digital_io/left_itb_button_right/state
/robot/digital_io/left_itb_button_up/state
/robot/digital_io/left_itb_light_inner/state
/robot/digital_io/left_itb_light_outer/state
/robot/digital_io/left_lower_button/state
/robot/digital_io/left_lower_cuff/state
/robot/digital_io/left_pneumatic/state
/robot/digital_io/left_shoulder_button/state
/robot/digital_io/left_suck/state
/robot/digital_io/left_upper_button/state
/robot/digital_io/limit_switch_1/state
/robot/digital_io/limit_switch_2/state
/robot/digital_io/limit_switch_3/state
/robot/digital_io/motor_fault_signal/state
/robot/digital_io/right_blow/state
/robot/digital_io/right_blue_light/state
/robot/digital_io/right_hand_camera_power/state
/robot/digital_io/right_itb_button0/state
/robot/digital_io/right_itb_button1/state
/robot/digital_io/right_itb_button2/state
/robot/digital_io/right_itb_button3/state
/robot/digital_io/right_itb_button_down/state
/robot/digital_io/right_itb_button_left/state
/robot/digital_io/right_itb_button_right/state
/robot/digital_io/right_itb_button_up/state
/robot/digital_io/right_itb_light_inner/state
/robot/digital_io/right_itb_light_outer/state
/robot/digital_io/right_lower_button/state
/robot/digital_io/right_lower_cuff/state
/robot/digital_io/right_pneumatic/state
/robot/digital_io/right_shoulder_button/state
/robot/digital_io/right_suck/state
/robot/digital_io/right_upper_button/state
/robot/digital_io/torso_brake/state
/robot/digital_io/torso_brake_sensor/state
/robot/digital_io/torso_camera_power/state
/robot/digital_io/torso_digital_input0/state
/robot/digital_io/torso_foot_pedal/state
/robot/digital_io/torso_left_itb_button0/state
/robot/digital_io/torso_left_itb_button1/state
/robot/digital_io/torso_left_itb_button2/state
/robot/digital_io/torso_left_itb_button3/state
/robot/digital_io/torso_left_itb_button_down/state
/robot/digital_io/torso_left_itb_button_left/state
/robot/digital_io/torso_left_itb_button_right/state
/robot/digital_io/torso_left_itb_button_up/state
/robot/digital_io/torso_left_itb_light_inner/state
/robot/digital_io/torso_left_itb_light_outer/state
/robot/digital_io/torso_process_sense0/state
/robot/digital_io/torso_process_sense1/state
/robot/digital_io/torso_right_itb_button0/state
/robot/digital_io/torso_right_itb_button1/state
/robot/digital_io/torso_right_itb_button2/state
/robot/digital_io/torso_right_itb_button3/state
/robot/digital_io/torso_right_itb_button_down/state
/robot/digital_io/torso_right_itb_button_left/state
/robot/digital_io/torso_right_itb_button_right/state
/robot/digital_io/torso_right_itb_button_up/state
/robot/digital_io/torso_right_itb_light_inner/state
/robot/digital_io/torso_right_itb_light_outer/state
/robot/digital_io/torso_safety_stop/state
/robot/digital_io/torso_ui_output0/state
/robot/digital_io/torso_ui_output1/state
/robot/digital_io/torso_ui_output2/state
/robot/digital_io/torso_ui_output3/state
/robot/digital_io_names
/robot/digital_io_states
/robot/end_effector/left_gripper/command
/robot/end_effector/left_gripper/configure
/robot/end_effector/left_gripper/properties
/robot/end_effector/left_gripper/rsdk/set_properties
/robot/end_effector/left_gripper/rsdk/set_state
/robot/end_effector/left_gripper/segment
/robot/end_effector/left_gripper/set_object_mass
/robot/end_effector/left_gripper/state
/robot/end_effector/right_gripper/command
/robot/end_effector/right_gripper/configure
/robot/end_effector/right_gripper/properties
/robot/end_effector/right_gripper/rsdk/set_properties
/robot/end_effector/right_gripper/rsdk/set_state
/robot/end_effector/right_gripper/segment
/robot/end_effector/right_gripper/set_object_mass
/robot/end_effector/right_gripper/state
/robot/end_effector_names
/robot/head/command_head_nod
/robot/head/command_head_pan
/robot/head/head_state
/robot/head/state
/robot/itb/left_itb/state
/robot/itb/right_itb/state
/robot/itb/torso_left_itb/state
/robot/itb/torso_right_itb/state
/robot/itb_names
/robot/itb_states
/robot/joint_names
/robot/joint_state_publish_rate
/robot/joint_states
/robot/limb/left/collision_avoidance_state
/robot/limb/left/collision_detection_state
/robot/limb/left/command_joint_position
/robot/limb/left/command_nullspace_setpoint_and_twist_stamped
/robot/limb/left/command_stiffness
/robot/limb/left/command_stiffness_limit
/robot/limb/left/command_twist_speed_limit
/robot/limb/left/command_twist_speed_limit_scale
/robot/limb/left/command_twist_stamped
/robot/limb/left/command_velocity_tozero
/robot/limb/left/commanded_endpoint_state
/robot/limb/left/endpoint_state
/robot/limb/left/gravity_compensation_torques
/robot/limb/left/inverse_dynamics_command
/robot/limb/left/joint_command
/robot/limb/left/joint_command_timeout
/robot/limb/left/limb_state
/robot/limb/left/set_damping_correction_weights
/robot/limb/left/set_dominance
/robot/limb/left/set_feed_forward_weights
/robot/limb/left/set_speed_ratio
/robot/limb/left/state
/robot/limb/left/stiffness
/robot/limb/left/stiffness_constraint
/robot/limb/left/suppress_body_avoidance
/robot/limb/left/suppress_collision_avoidance
/robot/limb/left/suppress_contact_safety
/robot/limb/left/suppress_cuff_interaction
/robot/limb/left/suppress_gravity_compensation
/robot/limb/left/twist_speed_constraint
/robot/limb/left/use_default_spring_model
/robot/limb/left/velocity_controller_state
/robot/limb/left/weight_integral_terms
/robot/limb/right/collision_avoidance_state
/robot/limb/right/collision_detection_state
/robot/limb/right/command_joint_position
/robot/limb/right/command_nullspace_setpoint_and_twist_stamped
/robot/limb/right/command_stiffness
/robot/limb/right/command_stiffness_limit
/robot/limb/right/command_twist_speed_limit
/robot/limb/right/command_twist_speed_limit_scale
/robot/limb/right/command_twist_stamped
/robot/limb/right/command_velocity_tozero
/robot/limb/right/commanded_endpoint_state
/robot/limb/right/endpoint_state
/robot/limb/right/gravity_compensation_torques
/robot/limb/right/inverse_dynamics_command
/robot/limb/right/joint_command
/robot/limb/right/joint_command_timeout
/robot/limb/right/limb_state
/robot/limb/right/set_damping_correction_weights
/robot/limb/right/set_dominance
/robot/limb/right/set_feed_forward_weights
/robot/limb/right/set_speed_ratio
/robot/limb/right/state
/robot/limb/right/stiffness
/robot/limb/right/stiffness_constraint
/robot/limb/right/suppress_body_avoidance
/robot/limb/right/suppress_collision_avoidance
/robot/limb/right/suppress_contact_safety
/robot/limb/right/suppress_cuff_interaction
/robot/limb/right/suppress_gravity_compensation
/robot/limb/right/twist_speed_constraint
/robot/limb/right/use_default_spring_model
/robot/limb/right/velocity_controller_state
/robot/limb/right/weight_integral_terms
/robot/limb_names
/robot/range/left_hand_range/state
/robot/range/right_hand_range/state
/robot/range_names
/robot/ref_joint_names
/robot/ref_joint_states
/robot/set_super_enable
/robot/set_super_reset
/robot/set_super_stop
/robot/sonar/head_sonar/approach_alert
/robot/sonar/head_sonar/lights/green_level
/robot/sonar/head_sonar/lights/red_level
/robot/sonar/head_sonar/lights/set_green_level
/robot/sonar/head_sonar/lights/set_lights
/robot/sonar/head_sonar/lights/set_red_level
/robot/sonar/head_sonar/lights/state
/robot/sonar/head_sonar/set_sonars_enabled
/robot/sonar/head_sonar/sonars_enabled
/robot/sonar/head_sonar/state
/robot/sonar_names
/robot/sonar_states
/robot/state
/robot/xdisplay
/robustcontroller/CalibrateArm/JointPoseAction/left/set_dc_weights
/robustcontroller/CalibrateArm/JointPoseAction/left/set_ff_weights
/robustcontroller/CalibrateArm/JointPoseAction/right/set_dc_weights
/robustcontroller/CalibrateArm/JointPoseAction/right/set_ff_weights
/robustcontroller/Tare/JointPoseAction/left/set_dc_weights
/robustcontroller/Tare/JointPoseAction/left/set_ff_weights
/robustcontroller/Tare/JointPoseAction/right/set_dc_weights
/robustcontroller/Tare/JointPoseAction/right/set_ff_weights
/robustcontroller/left/CalibrateArm/enable
/robustcontroller/left/CalibrateArm/status
/robustcontroller/left/Tare/enable
/robustcontroller/left/Tare/status
/robustcontroller/right/CalibrateArm/enable
/robustcontroller/right/CalibrateArm/status
/robustcontroller/right/Tare/enable
/robustcontroller/right/Tare/status
/rosout
/rosout_agg
/tf
/tf2_web_republisher/cancel
/tf2_web_republisher/feedback
/tf2_web_republisher/goal
/tf2_web_republisher/result
/tf2_web_republisher/status
/tf_static
/update/progress
/update/status
/usb/ready




baxter2control 288 indigo_ws $ rosnode list
/PositionKinematicsNode_left
/PositionKinematicsNode_right
/RethinkOut
/USBService
/base_to_world
/baxter_cams
/camera/camera_nodelet_manager
/camera/depth_metric
/camera/depth_metric_rect
/camera/depth_points
/camera/depth_rectify_depth
/camera/depth_registered_sw_metric_rect
/camera/driver
/camera/points_xyzrgb_sw_registered
/camera/register_depth_rgb
/camera/rgb_rectify_color
/collision/left/joint_collision_estimator_left
/collision/right/joint_collision_estimator_right
/diagnostic_aggregator
/rcloader_left
/rcloader_right
/realtime_loop
/ref_base_to_world
/robot/environment_server
/robot_ref_publisher
/robot_state_publisher
/rosapi
/rosbag_diagnostics
/rosbridge_websocket
/rosout
/rostopic_1606_1475778986421
/start_31966_1475775009747
/tf2_web_republisher
/xdisplay

baxter2control 289 indigo_ws $ rosnode info /camera/depth_rectify_depth
--------------------------------------------------------------------------------
Node [/camera/depth_rectify_depth]
Publications: 
 * /rosout [rosgraph_msgs/Log]

Subscriptions: None

Services: 
 * /camera/depth_rectify_depth/set_logger_level
 * /camera/depth_rectify_depth/get_loggers


contacting node http://baxter2control:57047/ ...
ERROR: Communication with node[http://baxter2control:57047/] failed!




baxter2control 306 indigo_ws $ roslaunch openni2_launch openni2.launch
... logging to /home/pontslerj/.ros/log/a9680030-8bf9-11e6-820f-90b11ca4bdef/roslaunch-baxter2control-5322.log
Checking log directory for disk usage. This may take awhile.
Press Ctrl-C to interrupt
Done checking log file disk usage. Usage is <1GB.

started roslaunch server http://baxter2control:54612/

SUMMARY
========

PARAMETERS
 * /camera/camera_nodelet_manager/num_worker_threads: 4
 * /camera/depth_rectify_depth/interpolation: 0
 * /camera/driver/auto_exposure: True
 * /camera/driver/auto_white_balance: True
 * /camera/driver/color_depth_synchronization: False
 * /camera/driver/depth_camera_info_url: 
 * /camera/driver/depth_frame_id: camera_depth_opti...
 * /camera/driver/depth_registration: False
 * /camera/driver/device_id: #1
 * /camera/driver/rgb_camera_info_url: 
 * /camera/driver/rgb_frame_id: camera_rgb_optica...
 * /rosdistro: indigo
 * /rosversion: 1.11.20

NODES
  /camera/
    camera_nodelet_manager (nodelet/nodelet)
    depth_metric (nodelet/nodelet)
    depth_metric_rect (nodelet/nodelet)
    depth_points (nodelet/nodelet)
    depth_rectify_depth (nodelet/nodelet)
    depth_registered_sw_metric_rect (nodelet/nodelet)
    driver (nodelet/nodelet)
    points_xyzrgb_sw_registered (nodelet/nodelet)
    register_depth_rgb (nodelet/nodelet)
    rgb_rectify_color (nodelet/nodelet)
  /
    camera_base_link (tf/static_transform_publisher)
    camera_base_link1 (tf/static_transform_publisher)
    camera_base_link2 (tf/static_transform_publisher)
    camera_base_link3 (tf/static_transform_publisher)

ROS_MASTER_URI=http://011305P0015:11311

core service [/rosout] found
process[camera/camera_nodelet_manager-1]: started with pid [5331]
process[camera/driver-2]: started with pid [5332]
process[camera/rgb_rectify_color-3]: started with pid [5333]
process[camera/depth_rectify_depth-4]: started with pid [5334]
process[camera/depth_metric_rect-5]: started with pid [5335]
process[camera/depth_metric-6]: started with pid [5336]
process[camera/depth_points-7]: started with pid [5343]
process[camera/register_depth_rgb-8]: started with pid [5347]
process[camera/points_xyzrgb_sw_registered-9]: started with pid [5353]
process[camera/depth_registered_sw_metric_rect-10]: started with pid [5361]
process[camera_base_link-11]: started with pid [5365]
process[camera_base_link1-12]: started with pid [5369]
process[camera_base_link2-13]: started with pid [5371]
process[camera_base_link3-14]: started with pid [5383]
[ INFO] [1475786847.408369405]: Initializing nodelet with 4 worker threads.
[ INFO] [1475786847.750986577]: Device "1d27/0601@3/40" found.
Warning: USB events thread - failed to set priority. This might cause loss of data...
[ INFO] [1475786929.785355839]: No matching device found.... waiting for devices. Reason: openni2_wrapper::OpenNI2Device::OpenNI2Device(const string&) @ /tmp/binarydeb/ros-indigo-openni2-camera-0.2.7/src/openni2_device.cpp @ 74 : Device open failed
	Could not open "1d27/0601@3/40": USB transfer timeout!



catkin_make
$ export ROS_HOSTNAME=localhost
$ export ROS_MASTER_URI=http://localhost:11311


to find out about the topic message type its: rostopic type /topic/
to see detail of the message use output of previous command
rosmsg show /output/
http://wiki.ros.org/ROS/Tutorials/UnderstandingTopics


roscore

baxter2control 967 ~ $ rostopic type /cameras/right_hand_camera/image
sensor_msgs/Image
baxter2control 968 ~ $ rosmsg show sensor_msgs/Image
std_msgs/Header header
  uint32 seq
  time stamp
  string frame_id
uint32 height
uint32 width
string encoding
uint8 is_bigendian
uint32 step
uint8[] data




.38 meters long
width of table 23.25 inches or .59 meters
.71 meters tall
.65 meters from arm to table

arm at mf.check_if_configureable([.78,0.0,.37],[0,0,-1],'left')

Table
table is at .37-.65 = -.28
floor is at .37+.65 = 1.02
points in pic= (76,99) (490,367) 
width 414 = .59
length 268 = .38
ratio 1 pixel = .00142


Sytrefoam
foam is at = -.92
height is .1
points (223,124) (416,266)
width 193 = 57
legth 142 = 41

gripper: 8.5 cm open
closed: 4.5 cm

ratio 1 pixel = .0029

.00142 at .65

.0029 at .92


.00548 slope
intercept -2893/1350000 -.00214296


python indigo_ws/src/baxter_projectyouth/src/baxter_projectyouth/start_baxter.py
python indigo_ws/src/baxter_projectyouth/src/baxter_proctyouth/get_left_hand_pic.py

starting position
.78,0.0,.37
close when below .085
rostopic to subscribe to is /robot/range/left_hand_range/state

/aruco_single/pose




Use aruco orientation for baxter-gripper orientation
lift up and down to avoide clutter
                                                                            
regular = [.686,.701,-.008,-.191]
upsidedown = [.704,-.700, .05, .05]
triangle pointed left = [.987,0,-.13,0]
triangle pointed right [0,.99,0,-.1]
upleft [.9,.4,0,0]
downleft [.93,-.3,-.1,0]

roslaunch reflex reflex_takktile.launch
rosservice call /reflex_takktile/calibrate_tactile
rosservice call /reflex_takktile/set_tactile_threshold "finger:
- sensor: [5, 5, 5, 5, 5, 5, 5, 5, 5]
- sensor: [5, 5, 5, 5, 5, 5, 5, 5, 5]
- sensor: [5, 5, 5, 5, 5, 5, 5, 5, 5]" 
rostopic pub /reflex_takktile/command_position reflex_msgs/PoseCommand "f1: 1.0
f2: 1.0
f3: 3.0
preshape: 0.0" 

rosservice call /reflex_takktile/enable_tactile_stops
rostopic pub /reflex_takktile/command_velocity reflex_msgs/VelocityCommand "f1: 1.0
f2: 1.0
f3: 1.0
preshape: 0.0"
rosservice call /reflex_takktile/disable_tactile_stops

rosservice call /reflex_takktile/calibrate_fingers

rostopic echo /reflex_takktile/hand_state/finger


criteria of which one to pick/grasp 1st
object with a flat top who's overall height is roughly the same size if not a little smaller than the middle link of the back will hold it.
Choose another block with similar properties also whose connecting side is parallel to the already held block.  Place on two frong fingers which will be extended to form a table.  Then close

marker 5 then 3

plan over baxter placement pose.

planning on pose Baxter
xyz corrdinate, rotation on gripper, place, let go.

planning on pose reflex hand
grip hard, grip to a certain strength

