import numpy as np
import math

def make_transform_from_vector(A,B):
     per = np.cross(A,B)
     angle = np.dot(A,B)
     s = np.sqrt(per[0]*per[0]+per[1]*per[1]+per[2]*per[2])
     temp=[[0,-per[2],per[1]],[per[2],0,per[0]],[per[1],per[0],0]]
     eye=[[1,0,0],[0,1,0],[0,0,1]]
     eye = np.asarray(eye)
     eye=np.asmatrix(eye)
     temp=np.asarray(temp)
     temp=np.asmatrix(temp)
     trans = eye + temp +((1-angle)/(s*s))*(temp*temp)
     return trans
      

def transform2quat( matrix ):
      """Construct quaternion from the transform/rotation matrix 
      :returns: quaternion formed from transform matrix
      :rtype: numpy array
      """

      # Code was copied from perl PDL code that uses backwards index ordering
      T = matrix.transpose()  
      den = np.array([ 1.0 + T[0,0] - T[1,1] - T[2,2],
                       1.0 - T[0,0] + T[1,1] - T[2,2],
                       1.0 - T[0,0] - T[1,1] + T[2,2],
                       1.0 + T[0,0] + T[1,1] + T[2,2]])
      
      max_idx = np.flatnonzero(den == max(den))[0]

      q = np.zeros(4)
      q[max_idx] = 0.5 * np.sqrt(max(den))
      denom = 4.0 * q[max_idx]
      if (max_idx == 0):
         q[1] =  (T[1,0] + T[0,1]) / denom 
         q[2] =  (T[2,0] + T[0,2]) / denom 
         q[3] = -(T[2,1] - T[1,2]) / denom 
      if (max_idx == 1):
         q[0] =  (T[1,0] + T[0,1]) / denom 
         q[2] =  (T[2,1] + T[1,2]) / denom 
         q[3] = -(T[0,2] - T[2,0]) / denom 
      if (max_idx == 2):
         q[0] =  (T[2,0] + T[0,2]) / denom 
         q[1] =  (T[2,1] + T[1,2]) / denom 
         q[3] = -(T[1,0] - T[0,1]) / denom 
      if (max_idx == 3):
         q[0] = -(T[2,1] - T[1,2]) / denom 
         q[1] = -(T[0,2] - T[2,0]) / denom 
         q[2] = -(T[1,0] - T[0,1]) / denom 

      return q

def normalise(array):
    sums = 0
    for i in range(len(array)):
         sums = sums + np.abs(array[i])*np.abs(array[i])
    #sums = np.abs(array[0])*np.abs(array[0])+np.abs(array[1])*np.abs(array[1])+np.abs(array[2])*np.abs(array[2])
    sums = math.sqrt(sums)
    for i in range(len(array)):
        array[i]=float(array[i])/sums
    return array

def other_attempt(v1,v2):
    v1 = normalise(v1)
    v2 = normalise(v2)
    eye=[[1,0,0],[0,1,0],[0,0,1]]
    eye = np.asarray(eye)
    eye=np.asmatrix(eye)
    v1 = np.asarray(v1)
    v2 = np.asarray(v2)
    if np.array_equal(v1,v2):
        return eye
    perp = np.cross(v1,v2)
    dot_prod = np.dot(v1,v2)
    angle = np.arccos(dot_prod)
    #print perp,dot_prod,angle
    if dot_prod == -1:
        #for i in range(3):
        #    if float(v2[i]) != float(v1[i]):
        #        eye[i,i]=-1
        eye = [[-1,0,0],[0,1,0],[0,0,-1]]
        return eye
    a = math.cos(angle/2.0)
    b, c, d = -perp*math.sin(angle/2.0)
    aa, bb, cc, dd = a*a, b*b, c*c, d*d
    bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
    trans = np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                     [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                     [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])
    trans[0] = normalise(trans[0])
    trans[1] = normalise(trans[1])
    trans[2] = normalise(trans[2])
    return trans

def q2trans(q):
    #den = abs(float(q[0])) + abs(float(q[1])) + abs(float(q[2])) +abs(float(q[3]))
    q = normalise(q)
    qx = q[0]
    qy = q[1]
    qz = q[2]
    qw = q[3]
    #print qx,qy,qz,qw
    
    return np.array([[1-2*qy*qy-2*qz*qz, 2*qx*qy-2*qz*qw, 2*qx*qz+2*qy*qw],
                     [2*qx*qy+2*qz*qw, 1-2*qx*qx-2*qz*qz, 2*qy*qz-2*qz*qw],
                     [2*qx*qz-2*qy*qw, 2*qy*qz+2*qx*qw, 1-2*qx*qx-2*qy*qy]])

def vec2trans(v1, v2):
    v1 = normalise(v1)
    v2 = normalise(v2)
    eye=[[1,0,0],[0,1,0],[0,0,1]]
    eye = np.asarray(eye)
    eye=np.asmatrix(eye)
    v1 = np.asarray(v1)
    v2 = np.asarray(v2)
    if np.array_equal(v1,v2):
        return eye
    vec = np.asarray([v2[0],0,v2[2]])
    zer=np.asarray([0,0,0])
    if np.array_equal(vec,zer):
         tran1 = other_attempt(v1,np.asarray([1,0,0]))
         tran2 = other_attempt(np.asarray([1,0,0]), np.asarray([0,v2[1],0]))
         print "tran1: ", tran1
         print "tran2: ", tran2
         return np.asmatrix(tran2)*np.asmatrix(tran1)
    tran1 = other_attempt(v1,normalise(vec))
    tran1 = np.asmatrix(tran1)
    vec2 = np.asarray([0,v2[1],0])
    if np.array_equal(vec2,zer):
         return tran1
    else:
         tran2 = np.asmatrix(other_attempt(vec,vec2))
         #print "tran1: ", tran1
         #print "tran2: ", tran2
         return tran2*tran1

def q2euler(q):
    x = math.atan2(2*(-q[0]*q[1]+q[2]*q[3]),1-2*(q[1]*q[1]+q[2]*q[2]))
    y = math.asin(2*(-q[0]*q[2]-q[3]*q[1]))
    z = math.atan2(2*(-q[0]*q[3]+q[1]*q[2]),1-2*(q[2]*q[2]+q[3]*q[3]))
    return x,y,z