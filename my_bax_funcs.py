import numpy as np
from baxter_projectyouth import my_funcs as mf
from baxter_pykdl import baxter_kinematics

_MIN_JOINTS = (np.pi/180)*np.array([-97.494,-123,-174.987,-2.864,-175.25,-90,-175.25])
_MAX_JOINTS = (np.pi/180)*np.array([97.494,60,174.987,150,175.25,120,175.25])
_JNT_RANGE = _MAX_JOINTS - _MIN_JOINTS
def check_if_configureable(position,vector,limb,flip = False):
    if flip:
         perp = np.asarray([[0,-1,0],[1,0,0],[0,0,1]])
         perp = np.asmatrix(perp)
    else:
         perp = np.asarray([[1,0,0],[0,1,0],[0,0,1]])
         perp = np.asmatrix(perp)
    vec = mf.normalise(vector)
    rotation = mf.vec2trans(np.asarray([0,0,1]),np.asarray(vec))
    print "rotation matrix: ",rotation
    rotation = np.asmatrix(rotation)*perp
    kin = baxter_kinematics(limb)
    orient = mf.transform2quat(rotation)
    print "quaternion: ", orient
    temp = kin.inverse_kinematics(position,orient)
    if (temp is not None):
        temp = temp*(180/np.pi)
    #print temp, type(temp), temp is not None
    attempt = 0
    while ((temp is None) & (attempt < 100)):
	# generate new seed
	seed = np.random.rand()*_JNT_RANGE + _MIN_JOINTS
        # shift the orientation around
        vec = np.asarray(vec)*4 + np.asarray([0,0,-1])
        vec = mf.normalise(vec)
        trans = mf.vec2trans([0,0,1],vec)
        trans = np.asmatrix(trans)
        q = mf.transform2quat(trans)
        temp = kin.inverse_kinematics(position,q, seed=seed)
        attempt = attempt + 1
        if (temp is not None):
            temp = temp*(180/np.pi)
    #temp = temp*(180/np.pi)
    return temp