from std_srvs.srv import Empty
import rospy
 
from reflex_msgs.msg import Command
from reflex_msgs.msg import PoseCommand
from reflex_msgs.msg import VelocityCommand
from reflex_msgs.msg import Hand
from reflex_msgs.msg import FingerPressure
from reflex_msgs.msg import ForceCommand
from reflex_msgs.srv import SetTactileThreshold, SetTactileThresholdRequest
import time
rospy.init_node('reflex_grasp')
# Services can automatically call hand calibration
calibrate_fingers = rospy.ServiceProxy('/reflex_takktile/calibrate_fingers', Empty)
calibrate_tactile = rospy.ServiceProxy('/reflex_takktile/calibrate_tactile', Empty)
 
# Services can set tactile thresholds and enable tactile stops
enable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/enable_tactile_stops', Empty)
disable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/disable_tactile_stops', Empty)
set_tactile_threshold = rospy.ServiceProxy('/reflex_takktile/set_tactile_threshold', SetTactileThreshold)
 
# This collection of publishers can be used to command the hand
command_pub = rospy.Publisher('/reflex_takktile/command', Command, queue_size=1)
pos_pub = rospy.Publisher('/reflex_takktile/command_position', PoseCommand, queue_size=1)
vel_pub = rospy.Publisher('/reflex_takktile/command_velocity', VelocityCommand, queue_size=1)
force_pub = rospy.Publisher('/reflex_takktile/command_motor_force', ForceCommand, queue_size=1)
 
# Constantly capture the current hand state
#rospy.Subscriber('/reflex_takktile/hand_state', Hand, hand_state_cb)
#force_pub.publish(ForceCommand(f1=250.0,f2=250.0,f3=250.0,preshape=0.0))


f1 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
f2 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
f3 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
print "reset"
pos_pub.publish(PoseCommand(f1=0.0,f2=0.0,f3=0.0,preshape=0.0))
time.sleep(3)
threshold = SetTactileThresholdRequest([f1, f2, f3])
set_tactile_threshold(threshold)
calibrate_fingers()
calibrate_tactile()
enable_tactile_stops()
print 'finished setting up'
time.sleep(3)
pos_pub.publish(PoseCommand(f1=0.0,f2=0.0,f3=0.0,preshape=0.0))
time.sleep(10)
pos_pub.publish(PoseCommand(f1=3.0,f2=3.0,f3=3.0,preshape=0.0))
print 'grab'
time.sleep(3)
disable_tactile_stops()
#force_pub.publish(ForceCommand(f1=250.0,f2=250.0,f3=250.0,preshape=0.0))
pos_pub.publish(PoseCommand(f1=3.0, f2=3.0, f3=3.0, preshape=0.0))
print 'tighten'
time.sleep(10)

print "place second object"
pos_pub.publish(PoseCommand(f1=0.0, f2=0.0, f3=3.0, preshape=0.0))
time.sleep(10)
enable_tactile_stops()
pos_pub.publish(PoseCommand(f1=3.0, f2=3.0, f3=3.0, preshape=0.0))
time.sleep(3)
disable_tactile_stops()
pos_pub.publish(PoseCommand(f1=3.0, f2=3.0, f3=3.0, preshape=0.0))
