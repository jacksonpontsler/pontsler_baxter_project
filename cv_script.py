import cv2
import math
import time

bax_arm = [.78,0.0,.37]
# read in the image
image = cv2.imread('testimage.jpg')
x = len(image)
y = len(image[0])
print image[0,0]
gray_i = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#find the chessboard so I can find that the table color is
ret,corners = cv2.findChessboardCorners(gray_i,(4,4))
# take the distance between chessboard squares so I can go off the chessboard and find the color
d1=corners[15][0][0]-corners[10][0][0]
d2=corners[15][0][1]-corners[10][0][1]
num1=math.ceil(-1.5*d1+corners[0][0][0])
num2=math.ceil(-1.5*d2+corners[0][0][1])
bgk=image[int(num2),int(num1)]
#bgk = image[210,320]
time.sleep(2)
print bgk


#table 422, 272

#find table
#table = gray_i
flag_beginning = True
for i in range(len(image)):
    for j in range(len(image[0])):
        #find a color in a close range of the table color
        if(math.sqrt(math.pow(image[i,j,0]-bgk[0],2)+math.pow(image[i,j,1]-bgk[1],2)+math.pow(image[i,j,2]-bgk[2],2)))<35.0:
            #table[i,j]=1
            last = (i,j)
            if (flag_beginning):
                flag_beginning = False
                first = (i,j)
    #print i

#backup to put in if you know where the table begins and ends in the picture
first = (94,102)
last = (360,517)

from collections import defaultdict
d = defaultdict(list)

# create dictionary of objects on table
import numpy
d[str(bgk)].append(first)
d[str(bgk)].append(last)
def check_range(key,color):
    for i in range(len(key)):
        #conversion for the string that was the key of the dicitionary back to array
        temp = key[i].replace('[','').replace(']','').replace(',','').split(' ')
        temp = map(int,temp)
        distance = math.sqrt(math.pow(temp[0]-color[0],2)+math.pow(temp[1]-color[1],2)+math.pow(temp[2]-color[2],2))
        if distance<60:
            #print "old color"
            return (temp)
    print "new color"
    color = map(int,color)
    return (color)

for i in range(first[0],last[0]):
    for j in range(first[1],last[1]):
        color = image[i,j]
        keys=list(d.keys())
        (key_color) = check_range(keys,color)
        d[str(key_color)].append((i,j))

keys = list(d.keys())

#create a new dictionary that has the average rgb values as keys
new_dic = {}

for i in range(len(keys)):
    temp = d[keys[i]]
    red = []
    green=[]
    blue=[]
    for j in range(len(temp)):
        color = image[temp[j][0],temp[j][1]]
        red.append(color[0])
        green.append(color[1])
        blue.append(color[2])
    # note for some reason get key as bgr this will flip it back to rbg
    new_dic[str([sum(blue)/len(blue),sum(green)/len(green),sum(red)/len(red)])]=d[keys[i]]
keys = list(new_dic.keys())
print keys


#find grasp able points from list in dictionary
#obj1 = new_dic[keys[5]]
#print obj1
#dic=defaultdict(list)
#for i in range(len(obj1)):
#    dic[str(obj1[i][0])].append(obj1[i][1])
#print dic
#print keys[5]
for i in range(len(keys)):
    if ((180,230) in new_dic[keys[i]]):
        index = i

#print keys[8], 'purple'
#print keys[9], 'red'
#print keys[5], 'yellow'
#print keys[4], 'green'
#print keys[4], 'blue'
print image[300,180] #blue
print image[175,350] #green
print image[300,300] #yellow
print image[180,230] #red
print image[170,170] #light purple
print image[250,400] #black
print image[277,403] #white

print keys[index], len(keys), index

##Find reachable points of a single object
##this is done by the dictionary of colors and points to a dictionary for a single
##color where the key is row and enteries are the columns
#obj= new_dic[keys[index]]
#dic=defaultdict(list)
#for i in range(len(obj)):
#    dic[str(obj[i][0])].append(obj[i][1])
#obj_key=dic.keys()
##find the distances of each list in dictionary
##find distance of baxter grippers open and closed
#distances = []
#for i in range(len(obj_key)):
#    obj_row = dic[obj_key[i]]
#    distances.append(abs(obj_row[0]-obj_row[len(obj_row)-1]))
#print distances
#
## need distance conversion from pixel to real world
## for tonight we'll say 25 to 50
## find largest stretch of rows that have the correct distances
#a_list = []
#sub_list =[]
#for i in range(len(distances)):
#    #print obj_key[i], distances[i]
#    if (distances[i]<50 and distances[i]>25):
#        print obj_key[i]
#       sub_list.append(int(obj_key[i]))
#    else:
#        a_list.append(sub_list)
#        sub_list = []
#a_list.append(sub_list)
##print a_list
#a_list = sorted(max(a_list,key=len))
#print a_list
#row = math.ceil(len(a_list)/2)
#row = a_list[int(row)]
#print row
##print dic
#obj_row = dic[str(row)]
##print obj_row
#col = math.ceil(obj_row[len(obj_row)-1]-abs(obj_row[0]-obj_row[len(obj_row)-1])/2)
#col = int(col)
#print row,col

def find_grasp_point(keys,index):
    #Find reachable points of a single object
    #this is done by the dictionary of colors and points to a dictionary for a single
    #color where the key is row and enteries are the columns
    obj= new_dic[keys[index]]
    dic=defaultdict(list)
    for i in range(len(obj)):
        dic[str(obj[i][0])].append(obj[i][1])
    obj_key=dic.keys()
    #find the distances of each list in dictionary
    #find distance of baxter grippers open and closed
    distances = []
    for i in range(len(obj_key)):
        obj_row = dic[obj_key[i]]
        distances.append(abs(obj_row[0]-obj_row[len(obj_row)-1]))
    #print distances
    # need distance conversion from pixel to real world
    # for tonight we'll say 25 to 50
    # find largest stretch of rows that have the correct distances
    a_list = []
    sub_list =[]
    for i in range(len(distances)):
        #print obj_key[i], distances[i]
        if (distances[i]<((8.5)*(414/59)) and distances[i]>((4.5)*(414/59))):
            print obj_key[i]
            sub_list.append(int(obj_key[i]))
        else:
            a_list.append(sub_list)
            sub_list = []
    a_list.append(sub_list)
    #print a_list
    a_list = sorted(max(a_list,key=len))
    print a_list
    if (not a_list) or len(a_list)<8:
        return(0,0)
    row = math.ceil(len(a_list)/2)
    row = a_list[int(row)]
    print row
    #print dic
    obj_row = dic[str(row)]
    #print obj_row
    col = math.ceil(obj_row[len(obj_row)-1]-abs(obj_row[0]-obj_row[len(obj_row)-1])/2)
    col = int(col)
    return (row,col)
pts=[]
for i in range(len(keys)):
    pts.append(find_grasp_point(keys,i))
    print pts[i]
print keys
print pts
print x,y
bax_2_pix = 59/414 
pix_2_bax = 414/59
# cut in half and use that
#x/2 = .37 bax_arm[0]
#y/2 = 0   bax_arm[1]
for i in range(len(pts)):
    if pts[i] ==(0,0):
        x = x
    else:
        (pt_x,pt_y) = pts[i]
        disx = pt_x-math.ceil(x/2)
        disy = pt_y-math.ceil(y/2)
        new_x = (.59/414)*disx + bax_arm[0]
        new_y = -(.38/268)*disy + bax_arm[1]
        print [new_x,new_y,.37]
        
# get the row and mid point between begin and end
# convert that to real world points

#have baxter move to that point with a high up z component
# use the io reading to see the distance to object
# incrementally move down till less than 5 cm
#from baxter_interface import AnalogIO
# to_go = AnalogIO.right_hand_range


