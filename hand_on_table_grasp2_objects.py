#! /usr/bin/python

# Grabbing an object with an aruco marker attached to it


# rospy for the subscriber
import rospy
# ROS Geometry PoseStamped message
from geometry_msgs.msg import PoseStamped
from baxter_projectyouth import util
from baxter_projectyouth import my_bax_funcs as mf
import baxter_interface
import time
import pickle
#rospy.init_node('aruco_listener')
util.connect_to_baxter('aruco_listener')
left = baxter_interface.Gripper('left')
left.calibrate()
f = file("src/baxter_projectyouth/src/baxter_projectyouth/has_object_pickle", 'rb')
_HAS_OBJECT = pickle.load(f)

#messages imported for reflex hand
from std_srvs.srv import Empty
from reflex_msgs.msg import Command
from reflex_msgs.msg import PoseCommand
from reflex_msgs.msg import VelocityCommand
from reflex_msgs.msg import Hand
from reflex_msgs.msg import FingerPressure
from reflex_msgs.msg import ForceCommand
from reflex_msgs.srv import SetTactileThreshold, SetTactileThresholdRequest
# Services can set tactile thresholds and enable tactile stops
enable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/enable_tactile_stops', Empty)
disable_tactile_stops = rospy.ServiceProxy('/reflex_takktile/disable_tactile_stops', Empty)
set_tactile_threshold = rospy.ServiceProxy('/reflex_takktile/set_tactile_threshold', SetTactileThreshold) 
# This collection of publishers can be used to command the hand
command_pub = rospy.Publisher('/reflex_takktile/command', Command, queue_size=1)
pos_pub = rospy.Publisher('/reflex_takktile/command_position', PoseCommand, queue_size=1)
vel_pub = rospy.Publisher('/reflex_takktile/command_velocity', VelocityCommand, queue_size=1)
force_pub = rospy.Publisher('/reflex_takktile/command_motor_force', ForceCommand, queue_size=1)



#predesign by mf.check_if_configureable([.78,0,.37],[0,0,-1],'left')
_DEFAULT_CAMERA = [-72.2460936674468,
 -23.66455075420931,
 61.80908196062283,
 3.955078120480664,
 -59.17236321363572,
 112.47802721522511,
 -22.89550778633807]


default_out_of_way_camera = [-18.21533201043595,
 -24.52148434698012,
 46.999511665045226,
 3.977050776705557,
 -37.06787105139378,
 109.73144518711354,
 -15.84228513814755]

right_out_of_way = [6.262207024094385,
 -14.721679670678029,
 63.76464836463826,
 35.31005855340238,
 -57.348632746969635,
 82.17773428109825,
 17.81982419838788]

#default_y = -.0144
default_y = -.017
default_x = .0051
if (_HAS_OBJECT):
    _DEFAULT_GOAL=[.68,-0.02,0.0]
    _DEFAULT_HEIGHT=.15
else:
    _DEFAULT_GOAL=[.68,0.05,0.0]
    _DEFAULT_HEIGHT=.15
_DEFAULT_TABLE_DOWN=-.1
_SPEED = .3


util.set_right_arm_speed(_SPEED)
util.set_left_arm_speed(_SPEED)

def reset():
    # get back to default
    left.open()
    print "moving back to default"
    #util.move_right_arm_to_positions(right_out_of_way)
    util.move_left_arm_to_positions(_DEFAULT_CAMERA)
    print "should be back at default"
    time.sleep(3)
    if(_HAS_OBJECT):
        f = file("src/baxter_projectyouth/src/baxter_projectyouth/has_object_pickle",'wb')
        pickle.dump(False,f,2)
    else:
        f = file("src/baxter_projectyouth/src/baxter_projectyouth/has_object_pickle",'wb')
        pickle.dump(True,f,2)
    rospy.signal_shutdown('finished protocol')

def move_arm(goal,vec,limb,perp=False):
    temp = mf.check_if_configureable(goal,vec,limb,perp)
    print temp
    if (temp is None):
        print "failed to meet desired workspace"
	reset()
    if (limb=='right'):
    	util.move_right_arm_to_positions(temp)
    else:
	util.move_left_arm_to_positions(temp)
    time.sleep(1.5)

def aruco_callback(msg):
    print("Attempting to move baxter")
    position = msg.pose.position
    quat = msg.pose.orientation
    new_x = .78 - 2.5*(-default_y+position.y)+.02
    new_y = 0 - 2.5*(-default_x+position.x)
    print new_x,new_y
    # go to object with left arm
    #print new_x,new_y
    util.move_left_arm_to_positions([-51,-17,-9,20,16,83,-24])
    move_arm([new_x,new_y,_DEFAULT_HEIGHT],[0,0,-1],'left')
    util.set_left_arm_speed(.1)
    move_arm([new_x,new_y,_DEFAULT_TABLE_DOWN],[0,0,-1],'left')
    left.close()
    time.sleep(.5)
    move_arm([new_x,new_y,_DEFAULT_HEIGHT],[0,0,-1],'left')
    util.set_left_arm_speed(_SPEED)
    # move object to new location
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_HEIGHT],[0,0,-1],'left',True)
    util.set_left_arm_speed(.1)
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_GOAL[2]],[0,0,-1],'left',True)
    left.open()
    util.set_left_arm_speed(_SPEED)
    move_arm([_DEFAULT_GOAL[0],_DEFAULT_GOAL[1],_DEFAULT_HEIGHT],[0,0,-1],'left',True)
    # now grap
    f1 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
    f2 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
    f3 = FingerPressure([20, 20, 20, 20, 20, 20, 20, 20, 20])
    threshold = SetTactileThresholdRequest([f1, f2, f3])
    set_tactile_threshold(threshold)
    pos_pub.publish(PoseCommand(f1=3.0,f2=3.0,f3=3.0,preshape=0.0))
    time.sleep(1)
    disable_tactile_stops()
    pos_pub.publish(PoseCommand(f1=3.0,f2=3.0,f3=3.0,preshape=0.0))
    time.sleep(.5)
    if (_HAS_OBJECT):
        pos_pub.publish(PoseCommand(f1=3.0,f2=3.0,f3=3.0,preshape=0.0))
    else:
        pos_pub.publish(PoseCommand(f1=0.0,f2=0.0,f3=3.0,preshape=0.0))
    time.sleep(2)
    reset()
    #global subscriber
    #subscriber.unregister()
    

def myhook():
    print "finished process"

def main():

    # Define your pose topic
    pose_topic = "/aruco_single/pose"
    # Set up your subscriber and define its callback
    rospy.Subscriber(pose_topic, PoseStamped, aruco_callback)
    #global subscriber 
    #subscriber= rospy.Subscriber(pose_topic, PoseStamped, aruco_callback)
    # Spin until ctrl + c
    rospy.spin()

if __name__ == '__main__':
    main()